class webserver
(
$user   = $webserver::params::user,
$group  = $webserver::params::group
) inherits webserver::params
{

  package { ['httpd', 'php', 'php-mysql', 'php-gd', 'php-xml', 'php-mbstring']:
    ensure => 'installed',
  }->
  service { 'httpd':
    enable => true,
  } ->
 file { '/var/www':
   ensure  => directory,
   owner   => $user,
   group   => $group,
   recurse => true,
  }
}
