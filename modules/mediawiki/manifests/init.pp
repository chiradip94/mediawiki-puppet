class mediawiki
(
  $user = $mediawiki::params::user,
  $group = $mediawiki::params::group,
  $temp_dir = $mediawiki::params::temp_dir,
  $app_name = $mediawiki::params::app_name,
  $db_server = $mediawiki::params::db_server,
  $db_name = $mediawiki::params::db_name,
  $db_un = $mediawiki::params::db_un,
  $db_pw = $mediawiki::params::db_pw,
  $public_ip = $mediawiki::params::public_ip,
  $site_name = $mediawiki::params::site_name,
  $ver = $mediawiki::params::ver,
  $url = $mediawiki::params::url,

) inherits mediawiki::params
{
   include mediawiki::setup
   include mediawiki::config
   Class[ "mediawiki::setup" ] ->
     Class[ "mediawiki::config" ]

}
