class mediawiki::setup
(
  $temp_dir = $mediawiki::temp_dir,
  $app_name = $mediawiki::app_name,
  $user     = $mediawiki::user,
  $group    = $mediawiki::group,
  $ver      = $mediawiki::ver,

)
{
  $vrsn = split($ver,'\.')
  $v1 = $vrsn[0]
  $v2 = $vrsn[1]
  $v3 = $vrsn[2]
  file { "${temp_dir}":
    ensure => directory
  }->
  file { "${temp_dir}/mediawiki.tar.gz":
    source  => "https://releases.wikimedia.org/mediawiki/${v1}.${v2}/mediawiki-${v1}.${v2}.${v3}.tar.gz",
  }->
  exec { "untar app":
    command => "tar -zxf ${temp_dir}/mediawiki.tar.gz",
    path    => '/usr/bin:/usr/sbin:/bin',
    cwd     => '/var/www'
  }->
  file { "/var/www/${app_name}":
    ensure => 'link',
    target => "/var/www/mediawiki-${v1}.${v2}.${v3}",
    owner  => "${user}",
    group  => "${group}",
  }->
  file { "/var/www/mediawiki-${v1}.${v2}.${v3}":
    ensure  => 'directory',
    owner   => "${user}",
    group   => "${group}",
    recurse => true,
  }
}

