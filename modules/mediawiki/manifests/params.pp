class mediawiki::params
{
$user = "apache"
$group = "apache"
$temp_dir = "/temp"
$app_name = "mediawiki"
$db_server = "dbserver"
$db_name = "dbName"
$db_un = "dbUn"
$db_pw = "dbPw"
$public_ip = "pubIp"
$site_name = "siteName"
$ver = "1.26.4"
$url = "dummy"
}
