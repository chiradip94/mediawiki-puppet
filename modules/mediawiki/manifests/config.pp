class mediawiki::config
(
  $user   = $mediawiki::user,
  $group  = $mediawiki::group,
  $ver      = $mediawiki::ver,
)
{
  $vrsn = split($ver,'\.')
  $v1 = $vrsn[0]
  $v2 = $vrsn[1]
  $v3 = $vrsn[2]

  file{ "/etc/httpd/conf/httpd.conf":
    ensure    => file,
    content   => template('mediawiki/httpd.conf.erb'),
    owner     => "${user}",
    group     => "${group}",
    notify    => Exec['restart httpd']
  }->
  file{ "/var/www/mediawiki-${v1}.${v2}.${v3}/LocalSettings.php":
    ensure    => file,
    content   => template('mediawiki/LocalSettings.php.erb'),
    owner     => "${user}",
    group     => "${group}"
  }
  exec{ 'restart httpd':
    command   => 'service httpd restart',
    path      => '/usr/bin:/usr/sbin:/bin'
  }
}

